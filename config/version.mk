# Copyright (C) 2016 The Pure Nexus Project
# Copyright (C) 2016 The JDCTeam
# Copyright (C) 2019 AOSkjP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

AOSKJP_MOD_VERSION = 2.0

#ifndef AOSKJP_BUILD_TYPE
    AOSKJP_BUILD_TYPE := BETA
#endif

DATE := $(shell date -u +%Y%m%d-%H%M)
TARGET_PRODUCT_SHORT := $(subst aoskjp_,,$(AOSKJP_BUILD_TYPE))

AOSKJP_VERSION := AOSkjP-$(AOSKJP_MOD_VERSION)-$(AOSKJP_BUILD_TYPE)-$(AOSKJP_BUILD)-$(DATE)
AOSKJP_FINGERPRINT := AOSkjP/$(AOSKJP_MOD_VERSION)/$(PLATFORM_VERSION)/$(TARGET_PRODUCT_SHORT)/$(DATE)

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
  ro.aoskjp.version=$(AOSKJP_VERSION) \
  ro.aoskjp.device=$(AOSKJP_BUILD) \
  ro.aoskjp.releasetype=$(AOSKJP_BUILD_TYPE) \
  ro.modversion=$(AOSKJP_MOD_VERSION)

AOSKJP_DISPLAY_VERSION := $(AOSKJP_MOD_VERSION)-$(AOSKJP_BUILD_TYPE)

PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
  ro.aoskjp.display.version=$(AOSKJP_DISPLAY_VERSION) \
  ro.aoskjp.fingerprint=$(AOSKJP_FINGERPRINT)  
